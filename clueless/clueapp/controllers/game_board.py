from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.views.generic import View

from clueapp.models import Game
from clueapp.helpers.notification import Notification

#class GameBoard(View):
#    template_name = 'clueapp/game_board.html'
    
#    def get(self, request, game_id):
#        game = get_object_or_404(Game, pk=game_id)
#        #TODO get game id from id
#        return render(request,
#                      self.template_name, game)

@login_required
def show_note(request,game_id):
    """This is a test for sending notification 

    TODO: Add the logic to return the notes
    """
    game = get_object_or_404(Game, pk=game_id)
    notification = Notification(game,
                                request.user)
    notification.notify_players(title="Test",
                                message="Display me")
    messages.info(request, "Notification sent")
    # return JsonResponse(json.dumps(data))
    return render(request, 'clueapp/game_board.html')

