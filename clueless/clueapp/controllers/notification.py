from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from django.http import JsonResponse
import json

from clueapp.models import Game
from clueapp.helpers.notification import Notification

@login_required
def get_notifications(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
    notification = Notification(game,
                                request.user)

    notes = notification.get_notification()
    data = []
    for note in notes:
        d = {'title': note.title,
             'message': note.message}
        data.append(d)
        notification.set_expire(note)
    return JsonResponse(json.dumps(data), safe=False)

    



