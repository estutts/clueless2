#from django.test import TestCase
#from .models import *
# Create your tests here.
# clear db: python3 manage.py sqlflush | python3 manage.py dbshell
# make admin: python3 manage.py createsuperuser   (, , clueless, clueless)
from clueapp.models import *

# Create users first
user1 = User.objects.create_user(username='user1', password='user1')
user2 = User.objects.create_user(username='user2', password='user2')
user3 = User.objects.create_user(username='user3', password='user3')
user4 = User.objects.create_user(username='user4', password='user4')
user5 = User.objects.create_user(username='user5', password='user5')
#user6 = User.objects.create_user(username='user6', password='user6')

# Create game
game = Game.objects.create(gameName='game1', state=0, creator=user1)

# Create suspects
scarlet = Suspect.objects.create(color=0)
plum = Suspect.objects.create(color=5)
mustard = Suspect.objects.create(color=1)
peacock = Suspect.objects.create(color=4)
green = Suspect.objects.create(color=3)
#white = Suspect.objects.create(color=2)

# Create players with users and join game
player1 = Player.objects.create(user=user1, game=game, name='user1', suspect=scarlet)
player2 = Player.objects.create(user=user2, game=game, name='user2', suspect=plum)
player3 = Player.objects.create(user=user3, game=game, name='user3', suspect=mustard)
player4 = Player.objects.create(user=user4, game=game, name='user4', suspect=peacock)
player5 = Player.objects.create(user=user5, game=game, name='user5', suspect=green)
#player6 = Player.objects.create(user=user6, game=game, name='user6', suspect=white)

