from django.apps import AppConfig


class ClueappConfig(AppConfig):
    name = 'clueapp'
