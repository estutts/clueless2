from django.contrib import admin

from .models import *

admin.site.register(Game)
admin.site.register(Player)
admin.site.register(Card)
admin.site.register(Hand)
admin.site.register(Board)
admin.site.register(Location)
admin.site.register(Suspect)
admin.site.register(Notepad)
