from django.db import models
from django.contrib.auth.models import User
import copy
import random

'''
card_dict = {0: 'Miss Scarlet', 1: 'Colonel Mustard', 2: 'Professor Plum',
             3: 'Mr. Green', 4: 'Mrs. White', 5: 'Mrs. Peacock', 6: 'Rope',
             7: 'Lead Pipe', 8: 'Knife', 9: 'Wrench', 10: 'Candlestick',
             11: 'Revolver', 12: 'Study', 13: 'Hall', 14: 'Lounge',
             15: 'Library', 16: 'Billard Room', 17: 'Dining Room',
             18: 'Conservatory', 19: 'Ballroom', 20: 'Kitchen'}
'''

SUSPECT = [(0, 'Miss Scarlet'), 
           (1, 'Colonel Mustard'), 
           (2, 'Professor Plum'),
           (3, 'Mr. Green'), 
           (4, 'Mrs. White'), 
           (5, 'Mrs. Peacock')]

COLORS = [
    (0, "Red"),
    (1, "Yellow"),
    (2, "White"),
    (3, "Green"),
    (4, "Blue"),
    (5, "Purple")
]

WEAPON = [(6, 'Rope'),
          (7, 'Lead Pipe'), 
          (8, 'Knife'), 
          (9, 'Wrench'), 
          (10, 'Candlestick'),
          (11, 'Revolver')]

ROOM = [(12, 'Study'), (13, 'Hall'), 
        (14, 'Lounge'), (15, 'Library'), 
        (16, 'Billiard Room'), (17, 'Dining Room'),
        (18, 'Conservatory'), (19, 'Ballroom'), 
        (20, 'Kitchen')]

CARDS = SUSPECT + WEAPON + ROOM

TYPES = (
    (0, 'Suspect'),
    (1, 'Weapon'),
    (2, 'Room')
)

DISPROVE_RESULT = [type_tuple for type_tuple in TYPES].append((3, 'Uncontested'))

class Game(models.Model):
    SUSPECT = [(0, 'Miss Scarlet'),
               (1, 'Colonel Mustard'),
               (2, 'Professor Plum'),
               (3, 'Mr. Green'),
               (4, 'Mrs. White'),
               (5, 'Mrs. Peacock')]

    WEAPON = [(6, 'Rope'),
              (7, 'Lead Pipe'),
              (8, 'Knife'),
              (9, 'Wrench'),
              (10, 'Candlestick'),
              (11, 'Revolver')]

    ROOM = [(12, 'Study'), (13, 'Hall'),
            (14, 'Lounge'), (15, 'Library'),
            (16, 'Billard Room'), (17, 'Dining Room'),
            (18, 'Conservatory'), (19, 'Ballroom'),
            (20, 'Kitchen')]

    CARDS = SUSPECT + WEAPON + ROOM

    STATES = (
        (0, 'pregame'),
        (1, 'move'),
        (2, 'suggest'),
        (3, 'disprove'),
        (4, 'accuse'),
        (5, 'postgame'),
        (6, 'ack_disprove')
    )

    state = models.PositiveSmallIntegerField(choices=STATES, null=True)
    gameName = models.CharField(max_length=32, null=True)
    caseFile = models.OneToOneField('Hand', null=True)
    board = models.OneToOneField('Board', null=True)
    currentPlayer = models.OneToOneField('Player', related_name='current', null=True)

    currentDisprover = models.OneToOneField('Player', related_name='disprover', null=True)
    suggestionWeapon = models.OneToOneField('Card', related_name='weapon', null=True)
    suggestionSuspect = models.OneToOneField('Card', related_name='suspect', null=True)
    disproveResult = models.PositiveSmallIntegerField(choices=DISPROVE_RESULT, null=True)

    creator = models.ForeignKey(User, null=True)

    def __str__(self):
        return "({}) {}".format(self.id, self.gameName)

    def getNextDisprover(self):
        i = self.currentDisprover.suspect.color

        for j in range(6):
            nextPlayer = [x for x in self.player_set.all() if x.suspect.color == (i + j + 1) % 6 and not x.dummy]
            if nextPlayer:
                return nextPlayer[0]
        return None  # game over

    def getNextPlayer(self):
        i = self.currentPlayer.suspect.color
        for j in range(6):
            nextPlayer = [x for x in self.player_set.all() if x.suspect.color == (i+j+1) % 6 and not x.dummy]
            if nextPlayer:
                return nextPlayer[0]
        return None  # game over

    def getFirstPlayer(self):
        for j in range(6):
            firstPlayer = [x for x in self.player_set.all() if x.suspect.color == j and not x.dummy]
            if firstPlayer:
                return firstPlayer[0]

    def getValidMoves(self):
        currentLocation = self.currentPlayer.location
        if currentLocation.isHallway():
            return currentLocation.connections.filter(location__lt=21)
        else:
            return [l for l in currentLocation.connections.filter(location__lt=21) if not l.player_set.all() or not l.isHallway()]

    def getValidMovesEx(self):
        currentLocation = self.currentPlayer.location
        if currentLocation.isHallway():
            valid_moves = currentLocation.connections.filter(location__lt=21)
        else:
            valid_moves = [l for l in currentLocation.connections.filter(location__lt=21) if not l.player_set.all() or not l.isHallway()]
            if not valid_moves:
                valid_moves = [currentLocation]
            elif self.currentPlayer.wasMoved:
                valid_moves.append(self.currentPlayer.location)
        return valid_moves

class Player(models.Model):
    user = models.ForeignKey(User, null=True)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    suspect = models.OneToOneField('Suspect', null=True)
    hand = models.OneToOneField('Hand', null=True)
    notepad = models.OneToOneField('Notepad', null=True)
    canHaveTurn = models.BooleanField(default=True)
    mustSuggest = models.BooleanField(default=False)
    location = models.ForeignKey('Location', null=True)
    wasMoved = models.BooleanField(default=False)
    dummy = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Card(models.Model):
    type = models.PositiveSmallIntegerField(choices=TYPES, null=True)
    card = models.PositiveSmallIntegerField(choices=CARDS, null=True)
    hand = models.ForeignKey('Hand', null=True)

    def __str__(self):
        return self.get_card_display()

    def getImagePath(self):
        imagePath = "".join([char for char in self.get_card_display().lower() if char != ' ' and char != '.'])
        return "pictures/cards/" + imagePath + '.png'

class Hand(models.Model):
    # name? can we have empty class for a container
    pass


class Board(models.Model):
    board_mapping = [[0, 1, 2, 3, 4],
                   [5, None, 6, None, 7],
                   [8, 9, 10, 11, 12],
                   [13, None, 14, None, 15],
                   [16, 17, 18, 19, 20]]

    def getPlayerInfo(self):
        board = self.location_set.order_by('location')
        player_info = copy.deepcopy(self.board_mapping)

        for j in range(5):
            for i in range(5):
                if player_info[i][j] != None:
                    player_info[i][j] = board[player_info[i][j]].player_set.all()
                if player_info[i][j] == []:
                    player_info[i][j] = None
        return player_info

    # null = blank space
    # location = location name
    def getLocationDisplays(self):
        board = self.location_set.order_by('location')
        location_displays = copy.deepcopy(self.board_mapping)

        for j in range(5):
            for i in range(5):
                if location_displays[i][j] != None:
                    location_displays[i][j] = board[location_displays[i][j]]
        return location_displays

    def getValidLocations(self):
        board = self.location_set.order_by('location')
        valid_locations = copy.deepcopy(self.board_mapping)
        valid_moves = self.game.getValidMovesEx()
        for j in range(5):
            for i in range(5):
                if valid_locations[i][j] != None:
                    if board[valid_locations[i][j]] in valid_moves:
                        valid_locations[i][j] = True
                    else:
                        valid_locations[i][j] = False
        return valid_locations


class Location(models.Model):
    LOCATIONS = (
        (0, 'Study'),           (1, 'Hallway1'),        (2, 'Hall'),            (3, 'Hallway2'),    (4, 'Lounge'),
        (5, 'Hallway3'),                                (6, 'Hallway4'),                            (7, 'Hallway5'),
        (8, 'Library'),         (9, 'Hallway6'),        (10, 'Billiard Room'),   (11, 'Hallway7'),   (12, 'Dining Room'),
        (13, 'Hallway8'),                               (14, 'Hallway9'),                           (15, 'Hallway10'),
        (16, 'Conservatory'),   (17, 'Hallway11'),      (18, 'Ballroom'),       (19, 'Hallway12'),  (20, 'Kitchen'),
        (21, 'Red Start'),      (22, 'Yellow Start'),   (23, 'White Start'),    (24, 'Green Start'),(25, 'Blue Start'),
        (26, 'Purple Start')
    )

    location = models.PositiveSmallIntegerField(choices=LOCATIONS, null=True)
    connections = models.ManyToManyField("self")
    board = models.ForeignKey(Board, null=True)

    def isHallway(self):
        if self.location in (1,3,5,6,7,9,11,13,14,15,17,19):
            return True
        else:
            return False

    def getImagePath(self):
        if self.isHallway():
            imagePath = 'hallway'
        else:
            imagePath = "".join([char for char in self.get_location_display().lower() if char != ' '])
        return "pictures/locations/" + imagePath + '.png'

    def __str__(self):
        return self.get_location_display()


class Suspect(models.Model):
    color = models.PositiveSmallIntegerField(choices=COLORS)
    #startingLocation = models.OneToOneField(Location)
    # image?

    def __str__(self):
        return self.get_color_display()


    def getImagePath(self):
        return "pictures/pieces/" + self.get_color_display().lower() + ".png"

class Notepad(models.Model):

    def getNotepadEntries(self):
        entries = []
        for card in CARDS:
            entries.append(self.entry_set.filter(card=card[0]).order_by('player'))
        return entries


class Entry(models.Model):
    MARKS = ((0, 'blank'),
             (1, 'x-mark'),
             (2, 'check-mark'))

    mark = models.PositiveSmallIntegerField(choices=MARKS, null=True)
    player = models.CharField(max_length=20, null=True)
    card = models.PositiveSmallIntegerField(choices=CARDS, null=True)
    notepad = models.ForeignKey(Notepad, null=True)
    color = models.CharField(max_length=20, null=True)

    def getCardDisplay(self):
        return '{:20}'.format(self.get_card_display())

    def getImagePath(self):
        return "pictures/notepad/" + self.get_mark_display() + ".png"

    def __str__(self):
        return self.player + ', ' + self.get_card_display() + ', ' + self.get_mark_display()


class NotificationModel(models.Model):
    title = models.CharField(max_length=256,
                                null=True)
    message = models.CharField(max_length=256, 
                                    null=True)
    destination = models.ForeignKey(Player, 
                                       null=True, 
                                       related_name='destination')
    origin = models.ForeignKey(Player, 
                                   null=True, 
                                   related_name='origin')
    expired = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_expired = models.DateTimeField(null=True)

