/**
 * This file defines all the game board functionality
 * 
 */
//Create Board
var b = jsboard.board({ attach: "game", size: "5x5", style:'checkerboard' });

//Define cell width and height
b.cell("each").style({width:"80px", height:"80px"});

//Define unclickable cells
b.cell([1,1]).style({background:"#000000"});
b.cell([1,3]).style({background:"#000000"});
b.cell([3,1]).style({background:"#000000"});
b.cell([3,3]).style({background:"#000000"});

// Define Room cells
b.cell([0,0]).style({text:"Study",  background:"url('study.png') no-repeat", width:"80px", height:"80px", margin:"0 auto" });
b.cell([0,2]).style({text:"hall",  background:"url('hall.png') no-repeat", width:"80px", height:"80px", margin:"0 auto" });
b.cell([0,4]).style({text:"lounge",  background:"url('lounge.png') no-repeat", width:"80px", height:"80px", margin:"0 auto" });
b.cell([2,0]).style({text:"library",  background:"url('library.png') no-repeat", width:"80px", height:"80px", margin:"0 auto" });
b.cell([2,2]).style({text:"billiard room",  background:"url('billiardroom.png') no-repeat", width:"80px", height:"80px", margin:"0 auto" });
b.cell([2,4]).style({text:"diningroom",  background:"url('diningroom.png') no-repeat", width:"80px", height:"80px", margin:"0 auto" });
b.cell([4,0]).style({text:"conservatory",  background:"url('conservatory.png') no-repeat", width:"80px", height:"80px", margin:"0 auto" });
b.cell([4,2]).style({text:"ballroom",  background:"url('ballroom.png') no-repeat", width:"80px", height:"80px", margin:"0 auto" });
b.cell([4,4]).style({text:"kitchen",  background:"url('kitchen.png') no-repeat", width:"80px", height:"80px", margin:"0 auto" });


//Setup Pieces

var red = jsboard.piece({text:"Miss Scarlet",  background:"url('red.png') no-repeat", width:"50px", height:"50px", margin:"0 auto" });
var yellow = jsboard.piece({text:"Colonel Mustard",  background:"url('yellow.png') no-repeat", width:"50px", height:"50px", margin:"0 auto" });
var purple   = jsboard.piece({text:"Professor Plum", background:"url('purple.png') no-repeat",width:"50px", height:"50px", margin:"0 auto" });
var green  = jsboard.piece({text:"Mr. Green",  background:"url('green.png') no-repeat", width:"50px", height:"50px", margin:"0 auto" });
var white   = jsboard.piece({text:"Mrs. White", background:"url('white.png') no-repeat", width:"50px", height:"50px", margin:"0 auto" });
var blue   = jsboard.piece({text:"Mrs. Peacock", background:"url('blue.png') no-repeat", width:"50px", height:"50px", margin:"0 auto" });
var placeholder1 = jsboard.piece({text:"PH", background:"url('PH.png') no-repeat", width:"50px", height:"50px", margin:"0 auto"});
var placeholder2 = jsboard.piece({text:"PH", background:"url('PH.png') no-repeat", width:"50px", height:"50px", margin:"0 auto"});
var placeholder3= jsboard.piece({text:"PH", background:"url('PH.png') no-repeat", width:"50px", height:"50px", margin:"0 auto"});
var placeholder4 = jsboard.piece({text:"PH", background:"url('PH.png') no-repeat", width:"50px", height:"50px", margin:"0 auto"});


//Create Pieces
var cluepieces = [red.clone(),
                  yellow.clone(),
                  purple.clone(),
                  green.clone(), 
                  white.clone(),
                  blue.clone(),
                  placeholder1.clone(),
                  placeholder2.clone(),
                  placeholder3.clone(),
                  placeholder4.clone()
                  ];


//Place Pieces
b.cell([0,3]).place(cluepieces[0]);
b.cell([1,4]).place(cluepieces[1]);
b.cell([1,0]).place(cluepieces[2]);
b.cell([4,1]).place(cluepieces[3]);
b.cell([4,3]).place(cluepieces[4]);
b.cell([3,0]).place(cluepieces[5]);
b.cell([1,1]).place(cluepieces[6]);
b.cell([1,3]).place(cluepieces[7]);
b.cell([3,1]).place(cluepieces[8]);
b.cell([3,3]).place(cluepieces[9]);

//Piece movement variables
var bindMoveLocs, bindMovePiece;

//Piece functionality
for (var i=0; i<6; i++) 
    cluepieces[i].addEventListener("click", function() { showMoves(this); });


//Show possible moves 
function showMoves(piece) {

    resetBoard();
        
    var thisPiece = b.cell(piece.parentNode).get();
    var newLocs = [];
    var loc;
    loc = b.cell(piece.parentNode).where();

  
    // Define Piece movement
    if (thisPiece=="red"||"yellow"||"green"||"purple"||"white"||"blue") {
        newLocs.push(
            [loc[0]-1,loc[1]],
            [loc[0]+1,loc[1]],
            [loc[0],loc[1]-1],
            [loc[0],loc[1]+1] 
       
        );
    }

    // Remove illegal moves 
    (function removeIllegalMoves(arr) {
        var fixedLocs = [];
        for (var i=0; i<arr.length; i++) 
            if (b.cell(arr[i]).get()==null)
                fixedLocs.push(arr[i]); 
        newLocs = fixedLocs;
    })(newLocs); 

    // Lock color spaces to movement of piece
    bindMoveLocs = newLocs.slice();
    bindMovePiece = piece; 
    bindMoveEvents(bindMoveLocs);

}

// Lock move to new locations
function bindMoveEvents(locs) {
    for (var i=0; i<locs.length; i++) {
        b.cell(locs[i]).DOM().classList.add("placementColor");
        b.cell(locs[i]).on("click", movePiece);  
    }
}

// Move the piece
function movePiece() {
    var userClick = b.cell(this).where();
    if (bindMoveLocs.indexOf(userClick)) {
        b.cell(userClick).place(bindMovePiece);
        resetBoard();
    }
}

// Remove placement color spaces and event listeners
function resetBoard() {
    for (var r=0; r<b.rows(); r++) {
        for (var c=0; c<b.cols(); c++) {
            b.cell([r,c]).DOM().classList.remove("placementColor");
            b.cell([r,c]).removeOn("click", movePiece);
        }
    }
}