from django.conf.urls import url
from django.contrib.auth.decorators import login_required
#from .controllers.game_board import GameBoard, show_note
from .controllers.notification import get_notifications
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<game_id>\d+)/$', views.game, name='game'),
    url(r'^(?P<game_id>\d+)/join_game/$', views.join_game, name='join_game'),
    
    url(r'^create_game/$', views.create_game, name='create_game'),
    url(r'^open_games/$', views.open_games, name='open_games'),
    url(r'^my_games/$', views.my_games, name='my_games'),
    url(r'^join_game_submit/$', views.join_game_submit, name='join_game_submit'),
    url(r'^players/$', views.players, name='players'),
    url(r'^game_contents/$', views.game_contents, name='game_contents'),
    url(r'^messages/$', views.message_list, name='messages'),
    url(r'^start_game/$', views.start_game, name='start_game'),
    url(r'^first_move/$', views.first_move, name='first_move'),
    url(r'^move/$', views.move, name='move'),
    url(r'^make_suggestion/$', views.make_suggestion, name='make_suggestion'),
    url(r'^disprove/$', views.disprove, name='disprove'),
    url(r'^make_accusation/$', views.make_accusation, name='make_accusation'),
    url(r'^end_turn/$', views.end_turn, name='end_turn'),
    url(r'^ack_disprove/$', views.ack_disprove, name='ack_disprove'),
    url(r'^change_notepad/$', views.change_notepad, name='change_notepad'),
    #TODO: add game id in gameboard
    url(r'^(?P<game_id>\d+)/game_board/$', views.game_board, name='game_board'),
#    url(r'^(?P<game_id>\d+)/gameboard/show_note/$', show_note, name='show_note'),
    #notification api
]
