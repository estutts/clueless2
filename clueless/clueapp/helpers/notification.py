from datetime import datetime 
from clueapp.models import NotificationModel

class Notification(object):
    def __init__(self, game, user):
        self.game = game
        self.user = user

    #TODO: add notification to client to restrict action
    # based on current player move
    def notify_players(self, title, message, players=None):
        if not players:
            players = self.game.player_set.all()

        for player in players:
            notification = NotificationModel\
                           .objects.create(title=title,
                                          message=message,
                                          destination=player,
                                          origin=self.user\
                                                     .player_set\
                                                     .get(game=self.game.id))

    def get_notification(self, user=None):
        if user == None:
            user = self.user
        return NotificationModel.\
               objects.filter(destination=self.user\
                                         .player_set\
                                         .get(game=self.game.id),
                              expired=False)\
               .order_by('date_created')


    def set_expire(self, notification):
        notification.expired = True
        notification.date_expired = datetime.now()
        notification.save()


