from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages


@login_required
def index(request):
    game_list = Game.objects.all()
    context = {'game_list': game_list}
    return render(request, 'clueapp/index.html', context)

@login_required
def game_board(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
    player_info = game.board.getPlayerInfo()
    display_info = game.board.getLocationDisplays()
    valid_moves = game.board.getValidLocations()
    board_info = [[(player_info[x][y], display_info[x][y], valid_moves[x][y]) for x in range(len(player_info))] for y in range(len(player_info[0]))]
    context = {'game': game, 'player_info': player_info, 'display_info': display_info, 'board_info' : board_info}
    return render(request, 'clueapp/game_board.html', context)


@login_required
def open_games(request):
    game_list = Game.objects.all()
    context = {'game_list': game_list}
    return render(request, 'clueapp/open_games.html', context)


@login_required
def my_games(request):
    game_list = Game.objects.all()
    context = {'game_list': game_list}
    return render(request, 'clueapp/my_games.html', context)


@login_required
def join_game(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
    if request.user.player_set.filter(game=game):
        messages.error(request, "You have already joined this game")
        return redirect('game', game_id=game.id)
    return render(request, 'clueapp/join_game.html', {'game': game})


@login_required
def create_game(request):
    input_name = request.POST['game_name']
    color = request.POST['color']
    if Game.objects.filter(gameName=input_name):
        messages.error(request, "Duplicate game name.")
    elif input_name and len(input_name) <= 32 and input_name.replace(' ', '').isalnum():
        # good input
        game = Game.objects.create(gameName=input_name, 
                                   state=0,
                                   creator=request.user)
        suspect = Suspect.objects.create(color=color)
        Player.objects.create(user=request.user, 
                              game=game, 
                              name=request.user.username, 
                              suspect=suspect)
        messages.success(request, "Your game has been created!")
        return redirect('game', game_id=game.id)
    else:
        # bad input
        messages.error(request, "Invalid game name.")
    return redirect('index')


@login_required
def join_game_submit(request):
    color = int(request.POST['color'])
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)
    if request.user.player_set.filter(game=game):
        messages.error(request, "You have already joined this game.")
        return redirect('index')
    elif game.player_set.count() >= 6:
        messages.error(request, "This game is full.")
        return redirect('index')
    elif color in [x.suspect.color for x in game.player_set.all()]:
        messages.error(request, "Sorry, that color is taken.")
        return redirect('join_game', game_id=game_id)
    else:
        suspect = Suspect.objects.create(color=color)
        Player.objects.create(user=request.user, game=game, name=request.user.username, suspect=suspect)
        messages.success(request, "You have joined the game")
    return redirect('game', game_id=game.id)


@login_required
def players(request):
    id = request.GET['id']
    game = get_object_or_404(Game, pk=id)
    context = {'game': game}
    return render(request, 'clueapp/players.html', context)

@login_required
def message_list(request):
    return render(request, 'clueapp/messages.html')


@login_required
def game_contents(request):
    id = request.GET['id']
    game = get_object_or_404(Game, pk=id)
    flags = {'game': game, 'hide_start': True, 'hide_move': True, 'hide_suggest': True, 'hide_accuse': True,
             'hide_disprove': True, 'hide_ack_disprove': True, 'first_move': True}

    # see if user is creator
    if request.user == game.creator:
        creator = True
    else:
        creator = False

    if game.state == 0:  # pregame
        if creator:
            flags['hide_start'] = False
            state_message = "You are the creator. Start the game when you have enough players."
        else:
            state_message = "This game has been created by '{0}. '\
                                        Waiting for {0} to start the game." \
                .format(game.creator.username)
        return render(request, 'clueapp/game.html', flags)

    # see if user is current player
    if request.user == game.currentPlayer.user:
        current_player = True
    else:
        current_player = False

    if game.state == 1:  # move
        if current_player:
            if game.currentPlayer.location.location < 21:
                flags['hide_move'] = False
                state_message = "It is your turn to move."
            else:
                flags['first_move'] = False
                state_message = "Move onto the board."
        else:
            state_message = "Waiting for {} to move.".format(game.currentPlayer)
    elif game.state == 2:  # suggest
        if current_player:
            flags['hide_suggest'] = False
            state_message = "It is your turn to suggest."
        else:
            state_message = "Waiting for {} to make a suggestion.".format(game.currentPlayer)
    elif game.state == 3:  # disprove
        state_message = "{} suggested {} in the {} with the {}.".format(game.currentPlayer.user.username,
                                                                                 game.suggestionSuspect,
                                                                                 game.currentPlayer.location,
                                                                                 game.suggestionWeapon)
        if request.user == game.currentDisprover.user:
            flags['hide_disprove'] = False
            state_message += " It is your turn to attempt to disprove."
        else:
            temp = " Waiting for {} to disprove the suggestion from {}".format(game.currentDisprover, game.currentPlayer)
            state_message += temp
    elif game.state == 4:  # accuse
        if current_player:
            flags['hide_accuse'] = False
            state_message = "It is your turn to accuse."
        else:
            state_message = "Waiting for {} to make an accusation.".format(game.currentPlayer)
    elif game.state == 5:  # postgame
        if current_player:
            state_message = "Congratulations! You have won the game!"
        else:
            state_message = "{} has won the game by correctly accusing: {}".format(game.currentPlayer,
                                                                                   game.caseFile.card_set.all())
    elif game.state == 6:  # acknowledge disprove
        if current_player:
            flags['hide_ack_disprove'] = False
            if game.disproveResult == 3:  # uncontested
                state_message = "No one could disprove your suggestion of {} in the {} with the {}".format(
                    game.suggestionSuspect,
                    game.currentPlayer.location,
                    game.suggestionWeapon)
            else:
                if game.disproveResult == 2:  # disproved with room
                    disproveCard = [card for card in CARDS if
                                    card.get_card_display() == game.currentPlayer.location.get_location_display()]
                elif game.disproveResult == 1:  # disproved with weapon
                    disproveCard = game.suggestionWeapon
                elif game.disproveResult == 0:  # disproved with suspect
                    disproveCard = game.suggestionSuspect
                    state_message = "{} disproved your suggestion of {} in the {} with the {} by showing the card for {}".format(
                        game.currentDisprover,
                        game.suggestionSuspect,
                        game.currentPlayer.location,
                        game.suggestionWeapon,
                        disproveCard)
        else:
            if game.disproveResult == 3:
                state_message = "No one could disprove the suggesion {} in the {} with the {}. Waiting for {} to acknowledge".format(
                    game.suggestionSuspect,
                    game.currentPlayer.location,
                    game.suggestionWeapon,
                    game.currentPlayer)
            else:
                state_message = "{} disproved the suggestion {} in the {} with the {}. Wating for acknowlegement from {}".format(
                    game.currentDisprover,
                    game.suggestionSuspect,
                    game.currentPlayer.location,
                    game.suggestionWeapon,
                    game.currentPlayer)
    player_info = game.board.getPlayerInfo()
    display_info = game.board.getLocationDisplays()
    valid_moves = game.board.getValidLocations()
    board_info = [[(player_info[x][y], display_info[x][y], valid_moves[x][y]) for y in range(len(player_info))] for x in
                  range(len(player_info[0]))]
    context = {'game': game, 'player_info': player_info, 'display_info': display_info, 'board_info': board_info}
    context.update(flags)
    context['state_message'] = state_message

    return render(request, 'clueapp/game_contents.html', context)


@login_required
def start_game(request):
    # grab game & players
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)

    # only start game if enough player.
    if game.player_set.count() < 3:
        messages.error(request, "Not enough players. Please invite more players to join")
        return redirect('game', game_id=game.id)
    
    # start game
    if game.state != 0:
        messages.error(request, "This game has already been started.")
        return redirect('game', game_id=game.id)

    game.state = 1
    game.save()

    # create cards, pick 3, shuffle, then distribute into hands
    cards = [i for i in range(21)]  # create deck
    suspect = random.randint(0, 5)  # pick suspect
    weapon = random.randint(6, 11)  # pick weapon
    room = random.randint(12, 20)  # pick room
    cards.remove(suspect)  # remove suspect from deck
    cards.remove(weapon)  # remove weapon from deck
    cards.remove(room)  # remove room from deck

    # create case file hand and cards for it
    caseFile = Hand.objects.create()
    Card.objects.create(type=0, card=suspect, hand=caseFile)
    Card.objects.create(type=1, card=weapon, hand=caseFile)
    Card.objects.create(type=2, card=room, hand=caseFile)
    game.caseFile = caseFile
    game.save()

    # create a hand for each player
    random.shuffle(cards)  # in place shuffle
    for i, player in enumerate(game.player_set.all()):
        hand = Hand.objects.create()
        player.hand = hand
        player.save()
        for j in range(i, len(cards), game.player_set.count()):
            if cards[j] in range(0, 5):
                typ = 0
            elif cards[j] in range(6, 11):
                typ = 1
            else:
                typ = 2
            Card.objects.create(type=typ, card=cards[j], hand=hand)

    # create notepad for each player
    for player in game.player_set.all():
        notepad = Notepad.objects.create()
        player.notepad = notepad
        player.save()
        for player in game.player_set.all():
            for card in CARDS:
                Entry.objects.create(player=player.name, card=card[0], mark=0, notepad=notepad, color=player.suspect.get_color_display())

    # need to create dummy players
    if game.player_set.count() < 6:
        missingSuspects = [x for x in range(6) if x not in [y.suspect.color for y in game.player_set.all()]]
        for i in missingSuspects:
            s = Suspect.objects.create(color=i)
            Player.objects.create(game=game, name='{} dummy{}'.format(game.gameName, i), suspect=s, dummy=True)

    # create a board, connect to game, and fill with locations
    board = Board.objects.create()
    game.board = board
    game.save()
    for i in range(27):
        Location.objects.create(board=board, location=i)

    # connect locations (needed to create them first)
    locations = board.location_set.order_by('location')
    locations[0].connections.add(locations[1], locations[5], locations[20])  # connect study with hallway1, hallway3, and kitchen
    locations[2].connections.add(locations[1], locations[3], locations[6])   # etc
    locations[4].connections.add(locations[3], locations[7], locations[16])
    locations[8].connections.add(locations[5], locations[9], locations[13])
    locations[10].connections.add(locations[6], locations[9], locations[11], locations[14])
    locations[12].connections.add(locations[7], locations[11], locations[15])
    locations[16].connections.add(locations[13], locations[17])
    locations[18].connections.add(locations[14], locations[17], locations[19])
    locations[20].connections.add(locations[15], locations[19])
    # now add starting locations
    locations[21].connections.add(locations[3])
    locations[22].connections.add(locations[7])
    locations[23].connections.add(locations[19])
    locations[24].connections.add(locations[17])
    locations[25].connections.add(locations[13])
    locations[26].connections.add(locations[5])


    # put players in starting location
    startLocations = [21, 22, 23, 24, 25, 26]
    for p in game.player_set.all():
        l = locations[startLocations[p.suspect.color]]
        l.player_set.add(p)

    game.currentPlayer = game.getFirstPlayer()
    game.save()

    return redirect('game', game_id=game.id)

@login_required
def game(request, game_id):
    """Displays the game board

        Only game creator can start a game. 
    """
    game = get_object_or_404(Game, pk=game_id)
    flags = {'game': game, 'hide_start': True, 'hide_move': True, 'hide_suggest': True, 'hide_accuse': True, 'hide_disprove': True, 'hide_ack_disprove': True, 'first_move': True}
    state_message = ''
    # see if user is creator
    if request.user == game.creator:
        creator = True
    else:
        creator = False

    if game.state == 0:  # pregame
        if creator:
            flags['hide_start'] = False
            state_message = "You are the creator. Start the game when you have enough players."
        else:
            state_message = "This game has been created by '{0}. '\
                                    Waiting for {0} to start the game." \
                                    .format(game.creator.username)
        return render(request, 'clueapp/game.html', flags)

    # see if user is current player
    if request.user == game.currentPlayer.user:
        current_player = True
    else:
        current_player = False

    if game.state == 1:  # move
        if current_player:
            if game.currentPlayer.location.location < 21:
                flags['hide_move'] = False
                state_message = "It is your turn to move."
            else:
                flags['first_move'] = False
                state_message = "Move onto the board."
        else:
            state_message = "Waiting for {} to move.".format(game.currentPlayer)
    elif game.state == 2:  # suggest
        if current_player:
            flags['hide_suggest'] = False
            state_message = "It is your turn to suggest."
        else:
            state_message = "Waiting for {} to make a suggestion.".format(game.currentPlayer)
    elif game.state == 3:  # disprove
        state_message = "{} suggested {} in the {} with the {}.".format(game.currentPlayer.user.username, game.suggestionSuspect, game.currentPlayer.location, game.suggestionWeapon)
        if request.user == game.currentDisprover.user:
            flags['hide_disprove'] = False
            state_message += " It is your turn to attempt to disprove."
        else:
            temp = " Waiting for {} to disprove the suggestion from {}".format(game.currentDisprover, game.currentPlayer)
            print(state_message)
            state_message += temp
    elif game.state == 4:  # accuse
        if current_player:
            flags['hide_accuse'] = False
            state_message = "It is your turn to accuse."
        else:
            state_message = "Waiting for {} to make an accusation.".format(game.currentPlayer)
    elif game.state == 5:  # postgame
        if current_player:
            state_message = "Congratulations! You have won the game!"
        else:
            state_message = "{} has won the game by correctly accusing: {}".format(game.currentPlayer, game.caseFile.card_set.all())
    elif game.state == 6: # acknowledge disprove
        if current_player:
            flags['hide_ack_disprove'] = False
            if game.disproveResult == 3: # uncontested
                state_message = "No one could disprove your suggestion of {} in the {} with the {}".format(game.suggestionSuspect,
                                                                                                                  game.currentPlayer.location,
                                                                                                                  game.suggestionWeapon)
            else:
                if game.disproveResult == 2: # disproved with room
                    disproveCard = [card for card in CARDS if card.get_card_display() == game.currentPlayer.location.get_location_display()]
                elif game.disproveResult == 1: # disproved with weapon
                    disproveCard = game.suggestionWeapon
                elif game.disproveResult == 0: # disproved with suspect
                    disproveCard = game.suggestionSuspect
                    state_message = "{} disproved your suggestion of {} in the {} with the {} by showing the card for {}".format(game.currentDisprover,
                                                                                                                                    game.suggestionSuspect,
                                                                                                                                    game.currentPlayer.location,
                                                                                                                                    game.suggestionWeapon,
                                                                                                                                    disproveCard)
        else:
            if game.disproveResult == 3:
                state_message = "No one could disprove the suggesion {} in the {} with the {}. Waiting for {} to acknowledge".format(game.suggestionSuspect,
                                                                                                                                            game.currentPlayer.location,
                                                                                                                                            game.suggestionWeapon,
                                                                                                                                            game.currentPlayer)
            else:
                state_message = "{} disproved the suggestion {} in the {} with the {}. Wating for acknowlegement from {}".format(game.currentDisprover,
                                                                                                                                        game.suggestionSuspect,
                                                                                                                                        game.currentPlayer.location,
                                                                                                                                        game.suggestionWeapon,
                                                                                                                                        game.currentPlayer)
    player_info = game.board.getPlayerInfo()
    display_info = game.board.getLocationDisplays()
    valid_moves = game.board.getValidLocations()
    board_info = [[(player_info[x][y], display_info[x][y], valid_moves[x][y]) for y in range(len(player_info))] for x in
                  range(len(player_info[0]))]
    context = {'game': game, 'player_info': player_info, 'display_info': display_info, 'board_info': board_info}
    context.update(flags)
    context['state_message'] = state_message

    return render(request, 'clueapp/game.html', context)

@login_required
def change_notepad(request):
    # grab game and player
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)
    player = request.user.player_set.get(game=game)

    # grab notepad entry
    notepad_player = request.POST['player']
    notepad_card = int(request.POST['card'])
    entry = player.notepad.entry_set.get(player=notepad_player, card=notepad_card)
    entry.mark = (entry.mark + 1) % 3
    entry.save()

    return redirect('game', game_id=game.id)



@login_required
def first_move(request):
    # grab game
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)
    if request.user != game.currentPlayer.user or game.state != 1:
        messages.error(request, "Not your turn to move.")
        return redirect('game', game_id=game.id)

    player = game.currentPlayer
    newLocation = player.location.connections.all()[0]  # should only be 1 connections
    currentLocation = player.location
    player.mustSuggest = False  # moving into hallway
    currentLocation.player_set.remove(player)
    newLocation.player_set.add(player)
    player.wasMoved = False  # first move cannot be from suggestion
    messages.success(request, "You have moved into the {}.".format(newLocation))
    messages.info(request, "Make a accusation or end your turn.")
    game.state = 4
    game.save()
    player.save()

    return redirect('game', game_id=game.id)


@login_required
def move(request):
    # grab game
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)
    if request.user != game.currentPlayer.user or game.state != 1:
        messages.error(request, "Not your turn to move.")
        return redirect('game', game_id=game.id)

    player = game.currentPlayer
    location = request.POST['newLocation']
    newLocation = get_object_or_404(Location, board=game.board, location=location)
    currentLocation = player.location
    validLocations = game.getValidMoves()
    player.mustSuggest = False

    if newLocation not in validLocations:
        if currentLocation == newLocation and player.wasMoved:
            player.wasMoved = False
            player.mustSuggest = True
            player.save()
            messages.success(request, "You have chosen to stay in the {}.".format(newLocation))
        elif not validLocations:
            player.wasMoved = False
            player.save()
            messages.success(request, "You have been blocked from leaving the {}.".format(newLocation))
        else:
            messages.error(request, "Invalid move.")
            return redirect('game', game_id=game.id)
    else:
        currentLocation.player_set.remove(player)
        newLocation.player_set.add(player)
        player.wasMoved = False
        messages.success(request, "You have moved into the {}.".format(newLocation))
        if not newLocation.isHallway():
            player.mustSuggest = True
        player.save()

    if player.mustSuggest:
        messages.info(request, "Make a suggestion.")
        game.state = 2
    else:
        messages.info(request, "Make a accusation or end your turn.")
        game.state = 4
    game.save()

    return redirect('game', game_id=game.id)

@login_required
def make_suggestion(request):

    # grab game
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)
    if request.user != game.currentPlayer.user or game.state != 2:
        messages.error(request, "Not your turn to make a suggestion")
        return redirect('game', game_id=game.id)

    # grab suggestion fields
    player = game.currentPlayer
    weapon = int(request.POST['weapon'])
    suspect = int(request.POST['suspect'])
    room = player.location

    if room.isHallway():
        messages.error(request, "You cannot make a suggestion in a hallway")
        return redirect('game', game_id=game.id)

    # save the suggestion weapon
    game.suggestionSuspect = Card.objects.create(type=0, card=suspect)
    game.suggestionWeapon = Card.objects.create(type=1, card=weapon)
    game.save()

    # move the suspect
    suspectPlayer = [player for player in game.player_set.all() if player.suspect.color == suspect][0]
    suspectLocation = suspectPlayer.location
    suspectLocation.player_set.remove(suspectPlayer)
    room.player_set.add(suspectPlayer)
    suspectPlayer.wasMoved = True
    game.save()

    # set game state to disprove
    game.currentDisprover = game.getNextPlayer()
    game.state = 3
    game.save()

    return redirect('game', game_id=game.id)

@login_required
def disprove(request):

    # grab game
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)
    if request.user != game.currentDisprover.user or game.state != 3:
        messages.error(request, "Not your turn to disprove a suggestion")
        return redirect('game', game_id=game.id)

    disprove = int(request.POST['disprove'])
    hasSuspect = False
    hasRoom = False
    hasWeapon = False

    if [card for card in game.currentDisprover.hand.card_set.all() if card.get_card_display() == game.suggestionSuspect.get_card_display()]:
        hasSuspect = True
    if [card for card in game.currentDisprover.hand.card_set.all() if card.get_card_display() == game.suggestionWeapon.get_card_display()]:
        hasWeapon = True
    if [card for card in game.currentDisprover.hand.card_set.all() if card.get_card_display() == game.currentPlayer.location.get_location_display()]:
        hasRoom = True

    # invalid because disprover lacks necessary card
    if disprove == 0 and not hasSuspect:
        messages.error(request, "You do not have the necessary suspect card")
        return redirect('game', game_id=game.id)
    elif disprove == 1 and not hasWeapon:
        messages.error(request, "You do not have the necessary weapon card")
        return redirect('game', game_id=game.id)
    elif disprove == 2 and not hasRoom:
        messages.error(request, "You cannot disprove with a card you do not have")
        return redirect('game', game_id=game.id)

    # catch valid and invalid passes
    elif disprove == 3:
        if hasSuspect or hasRoom or hasWeapon:
            messages.error(request, "You must disprove if you can")
            return redirect('game', game_id=game.id)

        # valid pass, check if no one could disprove
        else:
            if game.currentPlayer == game.getNextDisprover():
                game.state = 6
                game.disproveResult = disprove
                game.disproveAcknowledged = False
                game.save()
                return redirect('game', game_id=game.id)
            else:
                game.currentDisprover = game.getNextDisprover()
                game.save()
                return redirect('game', game_id=game.id)

    # disprover was able to disprove the suggestion
    else:
        game.state = 6
        game.disproveResult = disprove
        game.disproveAcknowledged = False
        game.save()
    return redirect('game', game_id=game.id)

def ack_disprove(request):
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)
    if request.user != game.currentPlayer.user or game.state != 6:
        messages.error(request, "You cannot acknowledge the result of a suggestion not your own")
        return redirect('game', game_id=game.id)

    game.state = 1
    game.save()
    return redirect('game', game_id=game.id)

@login_required
def make_accusation(request):

    # grab game
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)
    if request.user != game.currentPlayer.user or game.state != 4:
        messages.error(request, "Not your turn to make an accusation.")
        return redirect('game', game_id=game.id)

    # grab accusation fields
    player = game.currentPlayer
    weapon_guess = int(request.POST['weapon'])
    suspect_guess = int(request.POST['color'])
    room_guess = int(request.POST['room'])

    # grab case file fields
    suspect = game.caseFile.card_set.get(type=0).card
    weapon = game.caseFile.card_set.get(type=1).card
    room = game.caseFile.card_set.get(type=2).card

    # check if they are correct
    if suspect == suspect_guess and weapon == weapon_guess and room == room_guess:
        messages.success(request, "Congratulations! You have won the game!")
        game.state = 5
        game.save()
    else:
        messages.error(request, "I'm sorry, that accusation was incorrect. You can now only participate in disproving suggestions")
        player.canHaveTurn = False

        # move player into room, if needed
        currentLocation = player.location
        if currentLocation.isHallway():
            newLocation = Location.objects.get(location=0, board=game.board)  # defaults to Study
            currentLocation.player_set.remove(player)
            newLocation.player_set.add(player)
        player.save()

        # get next player, check if anyone is left first
        if game.player_set.filter(canHaveTurn=True, dummy=False).count() == 0:
            messages.error(request, "No players left, the game is over.")
            game.state = 5
        else:
            while game.currentPlayer.canHaveTurn == False:
                game.currentPlayer = game.getNextPlayer()
            game.state = 1
        game.save()

    return redirect('game', game_id=game.id)


@login_required
def end_turn(request):

    # grab game
    game_id = request.POST['game_id']
    game = get_object_or_404(Game, pk=game_id)
    if request.user != game.currentPlayer.user or game.state != 4:
        messages.error(request, "Not your turn to end your turn.")
        return redirect('game', game_id=game.id)

    # grab next player and state
    game.currentPlayer = game.getNextPlayer()
    while game.currentPlayer.canHaveTurn == False:
        game.currentPlayer = game.getNextPlayer()
    game.state = 1
    game.save()

    return redirect('game', game_id=game.id)
