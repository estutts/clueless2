from __future__ import absolute_import
from django.views import generic
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse_lazy
from .forms import CreateUserForm, LoginForm
from django.contrib.auth.models import User
from braces import views


# Home is now index in clueapp...
class HomePageView(generic.TemplateView):
    template_name = 'home.html'


class CreateUserView(views.AnonymousRequiredMixin, views.FormValidMessageMixin, generic.CreateView):
    form_class = CreateUserForm
    model = User
    template_name = 'accounts/createuser.html'
    form_valid_message = "Success! Welcome to Clue-Less!\n Please login with the username you just created."
    success_url = reverse_lazy('login')


class LoginView(views.AnonymousRequiredMixin, views.FormValidMessageMixin, generic.FormView):
    form_class = LoginForm
    success_url = reverse_lazy('home')
    template_name = 'accounts/login.html'
    form_valid_message = "Welcome back to Clue-Less!"

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None and user.is_active:
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)


class LogoutView(views.LoginRequiredMixin, views.MessageMixin, generic.RedirectView):
    url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        logout(request)
        self.messages.success("You've logged out of Clue-Less. Come back soon!")
        return super(LogoutView, self).get(request, *args, **kwargs)