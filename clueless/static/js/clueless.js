function getPlayers(){
    var res = document.location.pathname.split('/')
    var id = res[res.length-3] // hard coded
    $.get('/players/', {'id': id}, function(players){
        $('#player-list').html(players);
    });
}

function getGames(){
    $.get('/open_games/', function(games){ // this is hard coded...
        $('#open-game-list').html(games);
    });
    $.get('/my_games/', function(games){ // this is hard coded...
        $('#my-game-list').html(games);
    });
}

function getBoard(){
    var res = document.location.pathname.split('/')
    var id = res[res.length-2] // hard coded
    $.get('/game_contents/', {'id': id}, function(contents){
        $('#test').html(contents);
    });
}

function getMessages(){
    $.get('/messages/', function(){ // this is hard coded...
        $('#messages').html();
    });
}

$(function(){
    if(document.location.pathname.match(/^\/$/)){
        refreshTimer = setInterval(getGames, 5000);
    } else if(document.location.pathname.match(/\/([0-9]+)\/join_game\/$/)){
        refreshTimer = setInterval(getPlayers, 5000);
    } else if(document.location.pathname.match(/\/([0-9]+)\/$/)){
        refreshTimer = setInterval(getBoard, 5000);
        //refreshTimer = setInterval(getMessages, 5000);
    } else{

    }

});

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});